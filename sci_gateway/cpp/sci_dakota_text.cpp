#include "ParallelLibrary.H"
#include "ProblemDescDB.H"
#include "DakotaStrategy.H"
#include "DakotaModel.H"
#include "DakotaInterface.H"
//#include "PluginSerialDirectApplicInterface.H"
//#include "PluginParallelDirectApplicInterface.H"

#include <iostream>
#include <string>

#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"

using namespace std;

extern "C" void nidr_set_input_string(const char *);

extern "C" int sci_dakota_text(char *fname) 
{
  Dakota::Variables resultVar;
  Dakota::Response resultResp;

  SciErr sciErr;
  int i;
  int * pFilenameAddr = NULL;
  double * A_tmp;
  char * default_input;

  CheckRhs(1,1);
  CheckLhs(1,2);

  sciErr = getVarAddressFromPosition(pvApiCtx, 1, &pFilenameAddr);

  getAllocatedSingleString(pvApiCtx, pFilenameAddr, &default_input);

  // Instantiate/initialize the parallel library and problem description
  // database objects.
  Dakota::ParallelLibrary parallel_lib;
  Dakota::ProblemDescDB problem_db(parallel_lib);

  // specify_outputs_restart() is only necessary if specifying non-defaults
  //parallel_lib.specify_outputs_restart(NULL, NULL, NULL, NULL);

  // Instantiate the Strategy object (which instantiates all Model and
  // Iterator objects) using the parsed information in problem_db.
  Dakota::Strategy selected_strategy(problem_db);

  // serial default:
  nidr_set_input_string(default_input);

  // Execute the strategy
  problem_db.lock(); // prevent run-time DB queries
  selected_strategy.run_strategy();

  resultVar  = selected_strategy.variables_results();
  resultResp = selected_strategy.response_results();
  
  sciErr = allocMatrixOfDouble(pvApiCtx, Rhs + 1, resultVar.cv(), 1, &A_tmp);

  for(i=0; i<resultVar.cv();i++) A_tmp[i] = resultVar.continuous_variable(i);

  sciErr = allocMatrixOfDouble(pvApiCtx, Rhs + 2, resultResp.num_functions(), 1, &A_tmp);

  for(i=0; i<resultResp.num_functions();i++) A_tmp[i] = resultResp.function_value(i);

  LhsVar(1) = Rhs + 1; 
  LhsVar(2) = Rhs + 2; 
  
  freeAllocatedSingleString(default_input);

  return 0;
}
