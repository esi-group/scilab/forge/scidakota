#include "DakotaResponse.H"
#include "ParamResponsePair.H"
#include "system_defs.h"
#include "ProblemDescDB.H"
#include "ParallelLibrary.H"

#include "SciSerialDirectApplicInterface.h"

#include <api_scilab.h>
#include <call_scilab.h>
#include <MALLOC.h>
#include <stack-c.h>
#include <string.h>

/** fields to pass to Matlab in Dakota structure */
const char *SCI_FIELD_NAMES[] = { "dakota_type","numFns", "numVars", "numACV", "numADIV",  // 0
				  "numADRV", "numDerivVars", "xC", "xDI",    // 5
				  "xDR", "xCLabels", "xDILabels",            // 9
				  "xDRLabels", "directFnASV", "directFnASM", // 12
				  "directFnDVV", "directFnDVV_bool",         // 15
				  "fnFlag", "gradFlag", "hessFlag",          // 17
				  "fnVals",  "fnGrads",  "fnHessians",       // 20
				  "fnLabels", "failure", "fnEvalId" };     // 23 - Version 5.1 : fnEvalId - Newer version : currEvalId

/** number of fields in above structure */
const int SCI_NUMBER_OF_FIELDS = 26;

SciDirectApplicInterface::SciDirectApplicInterface(const Dakota::ProblemDescDB& problem_db) : Dakota::DirectApplicInterface(problem_db) 
{
  //this->init_serial();
}

int SciDirectApplicInterface::derived_map_ac(const Dakota::String& ac_name)
{
#ifdef MPI_DEBUG
    Cout << "analysis server " << analysisServerId << " invoking " << ac_name
         << " within SciDirectApplicInterface." << std::endl;
#endif // MPI_DEBUG

  int fail_code = 0;

  if (ac_name == "scilab")
    {
      const int SCILAB_FAIL = 1;
      const int RESPONSE_TYPES[] = {1, 2, 4};
      int i, j, k;

      /* temporary variables */
      double * x_tmp = NULL, dbl_tmp;
      std::string analysis_command;
      
      int fail_code;
      bool fn_flag;
  
      SciErr _SciErr;
      int * piMListAddr = NULL, * piLength = NULL;
      int piRows,piCols,piNbItem;
      char ** pstStrings = NULL;
  
      if (multiProcAnalysisFlag) 
	{
	  Cerr << "Error: The scilab direct fn interface does not yet support multiprocessor "
	       << "analyses." << std::endl;
	  Dakota::abort_handler(-1);
	}
      
      _SciErr = createMList(pvApiCtx, 1, SCI_NUMBER_OF_FIELDS, &piMListAddr);
      if (_SciErr.iErr)
	{
	  Cerr << "Error (Direct:Scilab): 1 "<< std::endl;
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      _SciErr = createMatrixOfStringInList(pvApiCtx, 1, piMListAddr, 1, 1, SCI_NUMBER_OF_FIELDS, SCI_FIELD_NAMES);
      if (_SciErr.iErr)
	{
	  Cerr << "Error (Direct:Scilab): 2 "<< std::endl;
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      // numFns
      dbl_tmp = numFns;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,2,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
	  
      // numVars
      dbl_tmp = numVars;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,3,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
	  
      // numACV
      dbl_tmp = numACV;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,4,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
	  
      // numADIV
      dbl_tmp = numADIV;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,5,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      // numADRV
      dbl_tmp = numADRV;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,6,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
	  
      // numDerivVars
      dbl_tmp = numDerivVars;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,7,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
	  
      /* continuous variables */

      // xC
      x_tmp = (double *)MALLOC(sizeof(double)*numACV);
      for(i=0;i<numACV;i++)
	{
	  x_tmp[i] = xC[i];
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,8,1,numACV,x_tmp);
      FREE(x_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      /* discrete integer variables */

      // xDI
      x_tmp = (double *)MALLOC(sizeof(double)*numADIV);
      for(i=0;i<numADIV;i++)
	{
	  x_tmp[i] = (double) xDI[i];
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,9,1,numADIV,x_tmp);
      FREE(x_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      // xDR 
      x_tmp = (double *)MALLOC(sizeof(double)*numADRV);
      for(i=0;i<numADRV;i++)
	{
	  x_tmp[i] = xDR[i];
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,10,1,numADRV,x_tmp);
      FREE(x_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      /* continuous labels */

      // xCLabels
      char ** pstStringsTmp = NULL;
      pstStringsTmp = (char **)MALLOC(sizeof(char*) * xCLabels.size());
  
      for(i = 0; i < xCLabels.size(); i++)
	{
	  pstStringsTmp[i] = (char *)xCLabels[i].c_str();
	}
  
      _SciErr = createMatrixOfStringInList(pvApiCtx,1,piMListAddr,11,1,xCLabels.size(),pstStringsTmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
      FREE(pstStringsTmp);

      /* discrete integer labels */

      // xDILabels
      pstStringsTmp = (char **)MALLOC(sizeof(char*) * xDILabels.size());
      for(i = 0 ; i < xDILabels.size() ; i++)
	{
	  pstStringsTmp[i] = (char *)xDILabels[i].c_str();
	}
  
      _SciErr = createMatrixOfStringInList(pvApiCtx,1,piMListAddr,12,1,xDILabels.size(),pstStringsTmp);
  
      FREE(pstStringsTmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      // xDRLabels
      pstStringsTmp = (char **)MALLOC(sizeof(char) * xDRLabels.size());
      for(i = 0 ; i < xDRLabels.size() ; i++)
	{
	  pstStringsTmp[i] = (char *)xDRLabels[i].c_str();
	}
  
      _SciErr = createMatrixOfStringInList(pvApiCtx,1,piMListAddr,13,1,xDRLabels.size(),pstStringsTmp);
  
      FREE(pstStringsTmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      /* active set vector (ASV) / matrix */

      // directFnASV
      x_tmp = (double *)MALLOC(sizeof(double)*numFns);
      for(i=0;i<numFns;i++)
	{
	  x_tmp[i] = directFnASV[i];
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,14,1,numFns,x_tmp);
      FREE(x_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
	  
      /* Create boolean version of ASV.  Rows are fnval, grad, hess; col per fn
	 CAREFUL -- Matlab stores by column */

      // directFnASM
      x_tmp = (double *)MALLOC(sizeof(double)*(3 * numFns));
      for( i=0; i<3; i++)
	{
	  for ( j=0; j<numFns; j++)
	    {
	      (directFnASV[j] & RESPONSE_TYPES[i])
		? (x_tmp[3*j+i] = 1)
		: (x_tmp[3*j+i] = 0);
	    }
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,15,3,numFns,x_tmp);
      FREE(x_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
	  
      /* derivative variables vector (DVV) / DVV_bool */

      // directFnDVV
      x_tmp = (double *)MALLOC(sizeof(double)*numDerivVars);
      for( i=0; i<numDerivVars; ++i)
	{
	  x_tmp[i] = directFnDVV[i];  
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,16,1,numDerivVars,x_tmp);
      FREE(x_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      // directFnDVV_bool
      x_tmp = (double *)MALLOC(sizeof(double)*numDerivVars);
      for ( j=0; j<numDerivVars; j++)
	{
	  x_tmp[directFnDVV[j] - 1] = 1;
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,17,1,numDerivVars,x_tmp);
      FREE(x_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      // fnFlag
      fn_flag = false;
      for ( j=0; j<numFns; ++j)
	{
	  if (directFnASV[j] & 1)
	    {
	      fn_flag = true;
	      break;
	    }
	}

      dbl_tmp = fn_flag;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,18,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
  
      // gradFlag
      dbl_tmp = gradFlag;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,19,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
  
      // hessFlag
      dbl_tmp = hessFlag;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,20,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}

      /* fn, grad, hess Flags; as needed, allocate & initialize matrices to zero */

      // fnVals
      if (fn_flag)
	{
	  x_tmp = (double *)MALLOC(sizeof(double)*numFns);
	  for(i=0;i<numFns;i++)
	    {
	      x_tmp[i] = 0.0;
	    }
	  _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,21,1,numFns,x_tmp);
	  FREE(x_tmp);
	  if (_SciErr.iErr)
	    {
	      printError(&_SciErr,0);
	      return (SCILAB_FAIL);
	    }
	}
      else
	{
	  dbl_tmp = 0;
	  _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,21,0,0,&dbl_tmp);
	  if (_SciErr.iErr)
	    {
	      printError(&_SciErr,0);
	      return (SCILAB_FAIL);
	    }
	}

      // fnGrads
      if (gradFlag)
	{
	  x_tmp = (double *)MALLOC(sizeof(double)*(numFns * numDerivVars));
	  for(i=0;i<numFns * numDerivVars;i++)
	    {
	      x_tmp[i] = 0.0;
	    }
	  _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,22,1,numFns * numDerivVars,x_tmp);
	  FREE(x_tmp);
	  if (_SciErr.iErr)
	    {
	      printError(&_SciErr,0);
	      return (SCILAB_FAIL);
	    }
	}
      else
	{
	  dbl_tmp = 0;
	  _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,22,0,0,&dbl_tmp);
	  if (_SciErr.iErr)
	    {
	      printError(&_SciErr,0);
	      return (SCILAB_FAIL);
	    }
	}

      // fnHessians
      if (hessFlag)
	{
	  int * piListInMList = NULL;

	  _SciErr = createListInList(pvApiCtx,1, piMListAddr, 23, numFns, &piListInMList);
	  for(i=0;i<numFns;i++)
	    {
	      dbl_tmp = 0;
	      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piListInMList,23,0,0,&dbl_tmp);
	      if (_SciErr.iErr)
		{
		  printError(&_SciErr,0);
		  return (SCILAB_FAIL);
		}
	    }
	}
      else
	{
	  dbl_tmp = 0;
	  _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,23,0,0,&dbl_tmp);
	  if (_SciErr.iErr)
	    {
	      printError(&_SciErr,0);
	      return (SCILAB_FAIL);
	    }
	}
  
      // fnLabels 
      pstStringsTmp = (char **)MALLOC(sizeof(char*) * xDRLabels.size());
      for(i = 0 ; i < xDRLabels.size() ; i++)
	{
	  pstStringsTmp[i] = (char *)xDRLabels[i].c_str();
	}
  
      _SciErr = createMatrixOfStringInList(pvApiCtx,1,piMListAddr,24,1,xDRLabels.size(),pstStringsTmp);
  
      FREE(pstStringsTmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
  
      // failure
      dbl_tmp = 0;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,25,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}


      // fnEvalId
      dbl_tmp = fnEvalId ;// Version 5.1 : fnEvalId - Newer version : currEvalId;
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,1,piMListAddr,26,1,1,&dbl_tmp);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
  
      /* put the structure into the Scilab workspace, then 
	 iterate over provided analysis components, checking for error each time */
  
      for (int aci=0;aci<analysisComponents[analysisDriverIndex].size();aci++)
	{
	  // strip away any .sci the user might have included
	  size_t pos = analysisComponents[analysisDriverIndex][aci].find(".");

	  int scistr_pos = 1, scistr_rhs = 1, scistr_lhs = 1;
	  SciString(&scistr_pos,(char *)analysisComponents[analysisDriverIndex][aci].substr(0,pos).c_str(),&scistr_rhs,&scistr_lhs);
	}
  
      /* retrieve and parse the response */
  
      _SciErr = getVarAddressFromPosition(pvApiCtx, 1, &piMListAddr);
      if (_SciErr.iErr)
	{
	  Cerr << "Error (Direct:Scilab): Failed to get variable Dakota from "
	       << "Scilab." << std::endl;
	  printError(&_SciErr,0);
	  return (SCILAB_FAIL);
	}
  
      // Readings matrix of strings
      _SciErr = getMatrixOfStringInList(pvApiCtx, piMListAddr, 1, &piRows, &piCols, NULL, NULL);
      piLength = (int*)MALLOC(sizeof(int) * piRows * piCols);
	  
      _SciErr = getMatrixOfStringInList(pvApiCtx, piMListAddr, 1, &piRows, &piCols, piLength, NULL);
  
      pstStrings = (char **)MALLOC(sizeof(char*) * piRows * piCols);
  
      for(i = 0 ; i < piRows * piCols ; i++)
	{
	  pstStrings[i] = (char*)MALLOC(sizeof(char) * (piLength[i] + 1));
	}
	  
      _SciErr = getMatrixOfStringInList(pvApiCtx, piMListAddr, 1, &piRows, &piCols, piLength, pstStrings);
	 
      if (strcmp(pstStrings[0],SCI_FIELD_NAMES[0]) != 0)
	{
	  return (SCILAB_FAIL);
	}
	  
      // Failure
      _SciErr = getMatrixOfDoubleInList(pvApiCtx, piMListAddr, 25, &piRows, &piCols, &x_tmp);
  
      if(_SciErr.iErr)
	{
	  printError(&_SciErr, 0);
	  return (SCILAB_FAIL);
	}
  
      if( *x_tmp != 0 )
	{
	  /* Scilab user indicated failure, don't process */
	  fail_code = (int) *x_tmp;
	}
      else
	{
	  fail_code = 0;
	  /* get fields by name in case the user somehow reordered, or decided to
	     only return some of the fields to us... update all of
	     fns:   1 x numFns
	     grads: numFns * numDerivVars
	     hess:  numFns * numDerivVars * numDerivVars
	     if any of these come back wrong, set fail_code
	  */    
	}
    
      if (fn_flag)
	{
	  _SciErr = getMatrixOfDoubleInList(pvApiCtx, piMListAddr, 21, &piRows, &piCols, &x_tmp);
      
	  if (x_tmp == NULL)
	    {
	  
	      Cerr << "Error (Direct:Scilab): Failed to get field fnVals from " 
		   << "Dakota structure." << std::endl;
	      fail_code = SCILAB_FAIL;
	  
	    }
	  else if (piRows != 1 | piCols != numFns )
	    {
	  
	      Cerr << "Error (Direct:Scilab): Dakota.fnVals must be [1 x numFns]." 
		   << std::endl;    
	      fail_code = SCILAB_FAIL;
	  
	    }
	  else
	    {
	      for (i=0; i<numFns; ++i)
		{
		  fnVals[i] += x_tmp[i];
		}   
	    }
	}
  
      if (gradFlag)
	{
	  _SciErr = getMatrixOfDoubleInList(pvApiCtx, piMListAddr, 22, &piRows, &piCols, &x_tmp);
	  if (_SciErr.iErr)
	    {
	      printError(&_SciErr, 0);
	      return (SCILAB_FAIL);
	    }

	  if ( piRows != numFns | piCols != numDerivVars )
	    {
	  
	      Cerr << "Error (Direct:Scilab): Dakota.fnVals must be "
		   << "[numFns x numDerivVars]." << std::endl;    
	      fail_code = SCILAB_FAIL;
	  
	    }
	  else 
	    {
	  
	      for (i=0; i<numFns; ++i)
		{
		  for (j=0; j<numDerivVars; ++j)
		    {
		      fnGrads(j,i) += x_tmp[numFns*j + i];
		    }
		}
	    }
	}
  
      // YC a revoir -> paramètre 23 list() contenant des matrices [NumDerivVarxNumDerivVar]
      if (hessFlag)
	{ 
	  _SciErr = getMatrixOfStringInList(pvApiCtx, piMListAddr, 23, &piRows, &piCols, NULL, NULL);
	  piLength = (int*)MALLOC(sizeof(int) * piRows * piCols);
      
	  _SciErr = getMatrixOfStringInList(pvApiCtx, piMListAddr, 23, &piRows, &piCols, piLength, NULL);
      
	  // YC faux ! CF plus bas ... ce n'est pas une matrice de strings ...
	  pstStrings = (char **)MALLOC(sizeof(char*) * piRows * piCols);
      
	  for(i = 0 ; i < piRows * piCols ; i++)
	    {
	      pstStrings[i] = (char*)MALLOC(sizeof(char) * (piLength[i] + 1));
	    }
      
	  _SciErr = getMatrixOfStringInList(pvApiCtx, piMListAddr, 23, &piRows, &piCols, piLength, pstStrings);
	  if (_SciErr.iErr) 
	    {
	      printError(&_SciErr, 0);
	      return (SCILAB_FAIL);
	    }
      
	  if ( *piLength != 3 )
	    {
	      Cerr << "Error (Direct:Scilab): Dakota.fnHessians must be "
		   << "3 dimensional." << std::endl;    
	      fail_code = SCILAB_FAIL;
	    }
	  else
	    {
	  
	      if ( *pstStrings[0] != numFns | *pstStrings[1] != numDerivVars | *pstStrings[2] != numDerivVars )
		{
	      
		  Cerr << "Error (Direct:Scilab): Dakota.fnHessians must be "
		       << "[numFns x numDerivVars x numDerivVars]." << std::endl;     
		  fail_code = SCILAB_FAIL;
	      
		}
	      else
		{
		  for (i=0; i<numFns; ++i)
		    {
		      for (j=0; j<numDerivVars; ++j)
			{
			  for (k=0; k<numDerivVars; ++k)
			    {
			      fnHessians[i](j,k) += *pstStrings[numDerivVars*numFns*k 
								+ numFns*j + i ];
			    }
			}
		    }
		}
	    }
	}
  
      /* get fnLabels--- optional return value */
      _SciErr = getMatrixOfStringInList(pvApiCtx, piMListAddr, 24, &piRows, &piCols, NULL, NULL);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr, 0);
	  return (SCILAB_FAIL);
	}
      piLength = (int*)MALLOC(sizeof(int) * piRows * piCols);
  
      _SciErr = getMatrixOfStringInList(pvApiCtx, piMListAddr, 24, &piRows, &piCols, piLength, NULL);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr, 0);
	  return (SCILAB_FAIL);
	}
      pstStrings = (char **)MALLOC(sizeof(char*) * piRows * piCols);
  
      for(i = 0 ; i < piRows * piCols ; i++)
	{
	  pstStrings[i] = (char*)MALLOC(sizeof(char) * (piLength[i] + 1));
	}
  
      _SciErr = getMatrixOfStringInList(pvApiCtx, piMListAddr, 24, &piRows, &piCols, piLength, pstStrings);
      if (_SciErr.iErr)
	{
	  printError(&_SciErr, 0);
	  return (SCILAB_FAIL);
	}
      for (i=0; i<numFns; ++i) 
	{
	  fnLabels[i] = pstStrings[i];
	}
  
      FREE(piLength);
      freeAllocatedMatrixOfString(piRows, piCols, pstStrings);
    }
  else
    {
      Cerr << ac_name << " is not available as an analysis within "
	   << "SciDirectApplicInterface." << std::endl;
      Dakota::abort_handler(-1);
    }
  
  // Failure capturing
  if (fail_code) throw fail_code;
  
  return 0;
}
