#include "ParallelLibrary.H"
#include "ProblemDescDB.H"
#include "DakotaStrategy.H"
#include "DakotaModel.H"
#include "DakotaInterface.H"

#include "SciSerialDirectApplicInterface.h"
//#include "PluginParallelDirectApplicInterface.H"

#include "sci_redirect.hpp"

#include <iostream>
#include <string>

#include <stack-c.h>
#include <api_scilab.h>
#include <Scierror.h>
#include <MALLOC.h>

using namespace std;

/** fields to pass to Scilab in Dakota structure */
const char *SCI_FIELD_NAMES_TOOLBOX[] = { "scidakota_type", "num_Functions", "num_Vars", "num_CV", "start_CV", // 0
					  "num_DIV", "start_DIV", "num_DRV", "start_DRV", "num_ICV", // 5
					  "start_ICV", "num_IDIV", "start_IDIV", "num_IDRV", "start_IDRV", // 10
					  "all_CV", "all_DIV", "all_DRV", "CV_Values", "CV_Labels", // 15
					  "DIV_Values", "DIV_Labels", "DRV_Values", "DRV_Labels", "ICV_Values", // 20
					  "ICV_Labels", "IDIV_Values", "IDIV_Labels", "IDRV_Values", "IDRV_Labels", // 25
					  "all_CV_Values", "all_CV_Labels", "all_DIV_Values", "all_DIV_Labels", "all_DRV_Values", // 30
					  "all_DRV_Labels", "response_Id", "request_Vector", "derivative_Vector", "fn_Labels", // 35
					  "fn_Values", "fn_Grads", "fn_Hessians"}; // 40

/** number of fields in above structure */
const int SCI_NUMBER_OF_FIELDS_TOOLBOX = 43;
const int SCILAB_FAIL = 1;

void model_interface_plugins(Dakota::ProblemDescDB& problem_db);
extern "C" int nidr_save_exedir(const char*, int);

extern "C" int sci_dakota_filename(char *fname) 
{
  Dakota::Variables resultVar;
  Dakota::Response resultResp;
  Dakota::ActiveSet ActiveSet;

  SciErr _SciErr;
  int i, j, k;
  int * pFilenameAddr = NULL, * piMListAddr = NULL, * piListGrad = NULL, * piListHess = NULL;
  double * A_tmp;
  double tmp_dbl;
  char * Filename;
  char ** tmp_str;
  char ** B_tmp;
  ScilabStream scicout(Cout);
  ScilabStream scicerr(Cerr);

  CheckRhs(1,1);
  CheckLhs(1,2);

  _SciErr = getVarAddressFromPosition(pvApiCtx, 1, &pFilenameAddr);

  getAllocatedSingleString(pvApiCtx, pFilenameAddr, &Filename);

  /** This function parses from an input file to define the ProblemDescDB data. */

  // Instantiate/initialize the parallel library and problem description
  // database objects.
  Dakota::ParallelLibrary parallel_lib;
  Dakota::ProblemDescDB problem_db(parallel_lib);

  if (parallel_lib.world_rank() == 0) cout << "dakota_filename: Library mode 1: run_dakota_parse()\n";
  
  // specify_outputs_restart() is only necessary if specifying non-defaults
  parallel_lib.specify_outputs_restart(NULL, NULL, NULL, NULL);
  
  // Manage input file parsing, output redirection, and restart processing
  // without a CommandLineHandler.  This version relies on parsing of an
  // input file.
  problem_db.manage_inputs(Filename);

  // Instantiate the Strategy object (which instantiates all Model and
  // Iterator objects) using the parsed information in problem_db.
  Dakota::Strategy selected_strategy(problem_db);

  // convenience function for iterating over models and performing any
  // interface plug-ins
  model_interface_plugins(problem_db);

  // Execute the strategy
  problem_db.lock(); // prevent run-time DB queries
  selected_strategy.run_strategy();

  resultVar  = selected_strategy.variables_results();
  resultResp = selected_strategy.response_results();
  
  
  _SciErr = createMList(pvApiCtx, Rhs+1, SCI_NUMBER_OF_FIELDS_TOOLBOX, &piMListAddr);
  if (_SciErr.iErr)
    {
      Cerr << "Error (Direct:Scilab): 1 "<< std::endl;
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }
  
  _SciErr = createMatrixOfStringInList(pvApiCtx, Rhs+1, piMListAddr, 1, 1, SCI_NUMBER_OF_FIELDS_TOOLBOX, SCI_FIELD_NAMES_TOOLBOX);
  if (_SciErr.iErr)
    {
      Cerr << "Error (Direct:Scilab): 2 "<< std::endl;
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }  
  
  tmp_dbl = (double) resultResp.num_functions();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,2,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }
  
  tmp_dbl = (double) resultVar.tv();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,3,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }
  
  tmp_dbl = (double) resultVar.cv();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,4,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }
  
  tmp_dbl = (double) resultVar.cv_start();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,5,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }
  
  tmp_dbl = (double) resultVar.div();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,6,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }
  
  tmp_dbl = (double) resultVar.div_start();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,7,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }
  
  tmp_dbl = (double) resultVar.drv();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,8,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }
  
  tmp_dbl = (double) resultVar.drv_start();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,9,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }

  tmp_dbl = (double) resultVar.icv();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,10,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }

  tmp_dbl = (double) resultVar.icv_start();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,11,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }

  tmp_dbl = (double) resultVar.idiv();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,12,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }

  tmp_dbl = (double) resultVar.idiv_start();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,13,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }

  tmp_dbl = (double) resultVar.idrv();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,14,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }

  tmp_dbl = (double) resultVar.idrv_start();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,15,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    } 
  
  tmp_dbl = (double) resultVar.acv();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,16,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }

  tmp_dbl = (double) resultVar.adiv();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,17,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }

  tmp_dbl = (double) resultVar.adrv();
  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,18,1,1,&tmp_dbl);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    }  

  if (resultVar.continuous_variables().length() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,19,0,0,A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,20,0,0,B_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultVar.cv()*sizeof(double));
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      for(i=0; i<resultVar.continuous_variables().length();i++)
	{
	  A_tmp[i] = (double) resultVar.continuous_variables()[i];
	  B_tmp[i] = (char *)resultVar.continuous_variable_labels()[i].c_str();
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,19,1,resultVar.continuous_variables().length(),A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,20,1,resultVar.cv(),B_tmp);
    }
  FREE(A_tmp);
  FREE(B_tmp);
  
  if (resultVar.discrete_int_variables().length() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,21,0,0,A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,22,0,0,B_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultVar.discrete_int_variables().length()*sizeof(double));
      B_tmp = (char **)MALLOC(resultVar.discrete_int_variables().length()*sizeof(char *));
      for(i=0; i<resultVar.discrete_int_variables().length();i++)
	{
	  A_tmp[i] = (double) resultVar.discrete_int_variables()[i];
	  B_tmp[i] = (char *)resultVar.discrete_int_variable_labels()[i].c_str();
	}

      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,21,1,resultVar.discrete_int_variables().length(),A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,22,1,resultVar.discrete_int_variables().length(),B_tmp);
    }
  FREE(A_tmp);
  FREE(B_tmp); 
  
  if (resultVar.discrete_real_variables().length() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,23,0,0,A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,24,0,0,B_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultVar.discrete_real_variables().length()*sizeof(double));
      B_tmp = (char **)MALLOC(resultVar.discrete_real_variables().length()*sizeof(char *));
      for(i=0; i<resultVar.discrete_real_variables().length();i++)
	{
	  A_tmp[i] = (double) resultVar.discrete_real_variables()[i];
	  B_tmp[i] = (char *) resultVar.discrete_real_variable_labels()[i].c_str();
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,23,1,resultVar.discrete_real_variables().length(),A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,24,1,resultVar.drv(),B_tmp);
    }
  FREE(A_tmp);
  FREE(B_tmp); 
  
  if (resultVar.inactive_continuous_variables().length() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,25,0,0,A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,26,0,0,B_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultVar.inactive_continuous_variables().length()*sizeof(double));
      B_tmp = (char **)MALLOC(resultVar.icv()*sizeof(char *));
      
      for(i=0;i<resultVar.inactive_continuous_variables().length();i++)
	{
	  A_tmp[i] = (double) resultVar.inactive_continuous_variables()[i];
	  B_tmp[i] = (char *) resultVar.inactive_continuous_variable_labels()[i].c_str();
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,25,1,resultVar.inactive_continuous_variables().length(),A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,26,1,resultVar.inactive_continuous_variables().length(),B_tmp);
    }
  FREE(A_tmp);
  FREE(B_tmp);

  if (resultVar.inactive_discrete_int_variables().length() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,27,0,0,A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,28,0,0,B_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultVar.inactive_discrete_int_variables().length()*sizeof(double));
      B_tmp = (char **)MALLOC(resultVar.idiv()*sizeof(char *));
      
      for(i=0;i<resultVar.inactive_discrete_int_variables().length();i++)
	{
	  A_tmp[i] = (double) resultVar.inactive_discrete_int_variables()[i];
	  B_tmp[i] = (char *)resultVar.inactive_discrete_int_variable_labels()[i].c_str();
	}
      
 
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,27,1,resultVar.idiv(),A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,28,1,resultVar.idiv(),B_tmp);
    }
  FREE(A_tmp);
  FREE(B_tmp);  

  if (resultVar.inactive_discrete_real_variables().length() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,29,0,0,A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,30,0,0,B_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultVar.inactive_discrete_real_variables().length()*sizeof(double));
      B_tmp = (char **)MALLOC(resultVar.idrv()*sizeof(char *));

      for(i=0;i<resultVar.inactive_discrete_real_variables().length();i++)
	{
	  A_tmp[i] = (double) resultVar.inactive_discrete_real_variables()[i];
	  B_tmp[i] = (char *)resultVar.inactive_discrete_real_variable_labels()[i].c_str();
	}
  
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,29,1,resultVar.inactive_discrete_real_variables().length(),A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,30,1,resultVar.idrv(),B_tmp);
    }
  FREE(A_tmp);
  FREE(B_tmp);  
  
  if (resultVar.all_continuous_variables().length() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,31,0,0,A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,32,0,0,B_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultVar.all_continuous_variables().length()*sizeof(double));
      B_tmp = (char **)MALLOC(resultVar.idrv()*sizeof(char *));
      
      for(i=0;i<resultVar.all_continuous_variables().length();i++)
	{
	  A_tmp[i] = (double) resultVar.all_continuous_variables()[i];
	  B_tmp[i] = (char *) resultVar.all_continuous_variable_labels()[i].c_str();
	}     
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,31,1,resultVar.all_continuous_variables().length(),A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,32,1,resultVar.all_continuous_variables().length(),B_tmp);
    }
  FREE(A_tmp);
  FREE(B_tmp);

  if (resultVar.all_discrete_int_variables().length() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,33,0,0,A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,34,0,0,B_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultVar.all_discrete_int_variables().length()*sizeof(double));
      B_tmp = (char **)MALLOC(resultVar.idrv()*sizeof(char *));

      for(i=0;i<resultVar.all_discrete_int_variables().length();i++)
	{	 
    	  A_tmp[i] = (double) resultVar.all_discrete_int_variables()[i];
	  B_tmp[i] = (char *) resultVar.all_discrete_int_variable_labels()[i].c_str();
	}
      
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,33,1,resultVar.all_discrete_int_variables().length(),A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,34,1,resultVar.all_discrete_int_variables().length(),B_tmp);
    }
  FREE(A_tmp);
  FREE(B_tmp);
  
  if (resultVar.all_discrete_real_variables().length() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,35,0,0,A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,36,0,0,B_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultVar.all_discrete_real_variables().length()*sizeof(double));
      B_tmp = (char **)MALLOC(resultVar.idrv()*sizeof(char *));

      for(i=0;i<resultVar.all_discrete_real_variables().length();i++)
	{
	  A_tmp[i] = (double) resultVar.all_discrete_real_variables()[i];
	  B_tmp[i] = (char *) resultVar.all_discrete_real_variable_labels()[i].c_str();
	}
          
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,35,1,resultVar.all_discrete_real_variables().length(),A_tmp);
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,36,1,resultVar.all_discrete_real_variables().length(),B_tmp);
    }
  FREE(A_tmp);
  FREE(B_tmp);
  
  tmp_str = (char **)MALLOC(1*sizeof(char *));

  tmp_str[0] = (char *) resultResp.responses_id().c_str();
  _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,37,1,1,tmp_str);
  if (_SciErr.iErr)
    {
      printError(&_SciErr,0);
      return (SCILAB_FAIL);
    } 
  
  if (ActiveSet.request_vector().size() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,38,0,0,A_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(ActiveSet.request_vector().size()*sizeof(double));
      for(i=0; i<ActiveSet.request_vector().size(); i++)
	{
	  A_tmp[i] = (double) ActiveSet.request_vector()[i];
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,38,1,ActiveSet.request_vector().size(),A_tmp);
    }
  FREE(A_tmp);

  if (ActiveSet.derivative_vector().size() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,39,0,0,A_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(ActiveSet.derivative_vector().size()*sizeof(double));
      for(i=0; i<ActiveSet.derivative_vector().size(); i++)
	{
	  A_tmp[i] = (double) ActiveSet.derivative_vector()[i];
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,39,1,ActiveSet.derivative_vector().size(),A_tmp);
    }
  FREE(A_tmp);
  
  if (resultResp.function_labels().size() == 0)
    {
      B_tmp = (char **)MALLOC(1*sizeof(char *));
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,40,0,0,B_tmp);
    }
  else
    {
      B_tmp = (char **)MALLOC(resultResp.function_labels().size()*sizeof(char *));
      for(i=0; i<resultResp.function_labels().size();i++)
	{
	  B_tmp[i] = (char *) resultResp.function_labels()[i].c_str();
	  }
      _SciErr = createMatrixOfStringInList(pvApiCtx,Rhs+1,piMListAddr,40,1,resultResp.function_labels().size(),B_tmp);
      }
  FREE(B_tmp);
  
  if (resultResp.function_values().length() == 0)
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,41,0,0,A_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultResp.function_values().length()*sizeof(double));
      for(i=0; i<resultResp.function_values().length();i++)
	{
	  A_tmp[i] = resultResp.function_values()[i];
	}
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,41,1,resultResp.function_values().length(),A_tmp);
    }
  FREE(A_tmp);
  
  if (resultResp.num_functions() == 0 || resultResp.function_gradient(0).length() == 0) // Y Chap : Gradient test
    {
      A_tmp = (double *)MALLOC(1*sizeof(double));
      _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,42,0,0,A_tmp);
    }
  else
    {
      A_tmp = (double *)MALLOC(resultVar.tv()*sizeof(double));
      _SciErr = createListInList(pvApiCtx,Rhs+1,piMListAddr,42,resultResp.num_functions(),&piListGrad);
      for(i=0; i<resultResp.num_functions();i++)
	{
	  for(j=0; j<resultVar.tv(); j++)
	    {
	      A_tmp[j] = resultResp.function_gradient(i)[j];
	    }
	  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piListGrad,i+1,resultVar.tv(),1,A_tmp);
	}
    }
  FREE(A_tmp);
  
  
  if (resultResp.num_functions() == 0 || resultResp.function_hessians().size() == 0)
   {
     A_tmp = (double *)MALLOC(1*sizeof(double));
     _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piMListAddr,43,0,0,A_tmp);
   }
  else
    {
      A_tmp = (double *)MALLOC((resultResp.num_functions()+resultVar.tv()*resultVar.tv())*sizeof(double));
      _SciErr = createListInList(pvApiCtx,Rhs+1,piMListAddr,43,resultResp.num_functions(),&piListHess);
      for(i=0; i<resultResp.num_functions();i++)
	{
	  for(j=0; j<resultResp.num_functions(); j++)
	    {
	      for(k=0; k<resultVar.tv(); k++)
		{
		  //A_tmp[j,k] = resultResp.function_hessian(i).values()[j,k]; // Y Chap : Hessian BUG 
		  A_tmp[j+resultVar.tv()*k] = resultResp.function_hessian(i).values()[j+resultVar.tv()*k];
		  //printf("DEBUG : %d \n\n",(j+resultVar.tv()*k));
		  //A_tmp[1,1]= resultResp.function_hessian(i).values()[1,1];
  		}
	    }
	  _SciErr = createMatrixOfDoubleInList(pvApiCtx,Rhs+1,piListHess,i+1,resultVar.tv()*resultVar.tv(),1,A_tmp);
	}
    }
  FREE(A_tmp);

  
  LhsVar(1) = Rhs + 1; 
  
  freeAllocatedSingleString(Filename);
  
  return 0;
}
